public class Vozilo {

    public Vozilo(String registracijskaOznaka, int brojSjedala, int brojVrata){
        this.registracijskaOznaka = registracijskaOznaka;
        this.brojSjedala = brojSjedala;
        this.brojVrata = brojVrata;
    }

    public int getBrojVrata() {
        return brojVrata;
    }

    public int getBrojSjedala() {
        return brojSjedala;
    }

    public String getRegistracijskaOznaka() {
        return registracijskaOznaka;
    }

    public void setBrojSjedala(int brojSjedala) {
        this.brojSjedala = brojSjedala;
    }

    public void setBrojVrata(int brojVrata) {
        this.brojVrata = brojVrata;
    }

    public void setRegistracijskaOznaka(String registracijskaOznaka) {
        this.registracijskaOznaka = registracijskaOznaka;
    }

    String registracijskaOznaka;
    int brojSjedala;
    int brojVrata;

}
