public class Automobil extends Vozilo implements iVaranje{

    public Automobil(String marka, double zapremninaGoriva){
        super("DA-324-BZ",4,5);
        this.marka=marka;
        this.zapremninaGoriva=zapremninaGoriva;

    }

    public double getZapremninaGoriva() {
        return zapremninaGoriva;
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public void setZapremninaGoriva(double zapremninaGoriva) {
        this.zapremninaGoriva = zapremninaGoriva;
    }
    public void letenje(){
        System.out.println("auto leti . . .");

    }
    public void kretanjePoVodi(){
        System.out.println("auto hoda po vodi . . .");

    }

    String marka;
    double zapremninaGoriva;

}

